<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class user_table extends Model
{
    public $table= 'user_table';
    public $primaryKey='id';
    public $incrementing = true;
    public $timestamps= false;

}
